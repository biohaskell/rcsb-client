-----------------------------------------------------------------------------
-- |
-- Module     : RCSB
-- Copyright  : (c) Daniel Firth 2018
-- License    : BSD3
-- Maintainer : locallycompact@gmail.com
-- Stability  : experimental
--
-- This file defines general functions for interacting with the RCSB PDB API
-- (https://www.rcsb.org/pdb/software/rest.do)
--
-----------------------------------------------------------------------------
module RCSB where

import           Data.Yaml
import           Network.HTTP.Simple
import           Network.HTTP.Conduit
import           Network.HTTP.Types
import           RIO
import qualified RIO.Text                      as Text

rcsbBaseUrl = "https://www.rcsb.org"

rcsbRequestJson
  :: (MonadIO m, MonadThrow m, FromJSON a)
  => ByteString
  -> ByteString
  -> RequestBody
  -> m a
rcsbRequestJson method path body = do
  request' <- parseRequest . Text.unpack $ rcsbBaseUrl
  let request =
        setRequestMethod method
          $ setRequestPath ("pdb/json/" <> path)
          $ setRequestBody body request'
  response <- httpJSON request
  return $ getResponseBody response
